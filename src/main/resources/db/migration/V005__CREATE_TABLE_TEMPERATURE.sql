CREATE TABLE temperature (
                       id long NOT NULL AUTO_INCREMENT,
                       value int,
                       date_in timestamp,
                       time_in varchar(100)
);