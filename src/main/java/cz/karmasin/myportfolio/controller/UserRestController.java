package cz.karmasin.myportfolio.controller;

import cz.karmasin.myportfolio.model.entity.ResultTemp;
import cz.karmasin.myportfolio.model.entity.Temperature;
import cz.karmasin.myportfolio.model.entity.UserHasTemperature;
import cz.karmasin.myportfolio.model.repository.TemperatureRepository;
import cz.karmasin.myportfolio.model.repository.UserHasTemperatureRepository;
import cz.karmasin.myportfolio.service.UserRestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@RestController
public class UserRestController {
    private final UserHasTemperatureRepository userHasTemperatureRepository;
    private final TemperatureRepository temperatureRepository;
    private final UserRestService userRestService;

    public UserRestController(UserHasTemperatureRepository userHasTemperatureRepository, TemperatureRepository temperatureRepository, UserRestService userRestService) {
        this.userHasTemperatureRepository = userHasTemperatureRepository;
        this.temperatureRepository = temperatureRepository;
        this.userRestService = userRestService;
    }


    @GetMapping("/list-t")
    public List<Temperature> showAllUserTemperaturesJson() {

        Iterable<UserHasTemperature> list = userHasTemperatureRepository.findAll();

        List<Temperature> result = new ArrayList<>();

        for (UserHasTemperature str : list) {
            result.add(temperatureRepository.findById(str.getId()).get());
        }

        return result;
    }

    @GetMapping("/list-t/longest-interval/{tempA}/{tempB}")
    public List<ResultTemp> daysLongestInterval(@PathVariable int tempA, @PathVariable int tempB) throws ParseException {
        List<ResultTemp> list = userRestService.getDaysInLongestInterval(tempA,tempB);
        return list;
}

    @GetMapping("/list-t/longest-interval-sum/{tempA}/{tempB}")
    public Integer sumOfDaysLongestInterval(@PathVariable int tempA, @PathVariable int tempB) throws ParseException {
        Integer days = userRestService.getSumDaysForTemperature(tempA,tempB);
        return days;
    }

}
