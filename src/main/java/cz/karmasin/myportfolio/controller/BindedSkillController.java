package cz.karmasin.myportfolio.controller;

import cz.karmasin.myportfolio.dto.DtoUserHasSkill;
import cz.karmasin.myportfolio.model.entity.UserHasSkill;
import cz.karmasin.myportfolio.model.repository.SkillRepository;
import cz.karmasin.myportfolio.model.repository.UserHasSkillRepository;
import cz.karmasin.myportfolio.model.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
public class BindedSkillController {

    private final UserHasSkillRepository userHasSkillRepository;
    private final SkillRepository skillRepository;
    private final UserRepository userRepository;

    public BindedSkillController(UserHasSkillRepository userHasSkillRepository, SkillRepository skillRepository, UserRepository userRepository) {
        this.userHasSkillRepository = userHasSkillRepository;
        this.skillRepository = skillRepository;
        this.userRepository = userRepository;
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException() {
        return "error-id";
    }


    @GetMapping(value = {"/", "/binded-list"})
    public String showAllUsersWithSkill(Model model) {
        model.addAttribute("bindedList", userHasSkillRepository.findAll());
        log.debug("List of all users with skills loaded");
        return "binded-list";
    }

    @GetMapping(value = {"/binded-form", "/binded-form/{id}"})
    public String showBindedForm(@PathVariable(required = false) Long id, Model model) {

        model.addAttribute("skillList", skillRepository.findAll());
        model.addAttribute("userList", userRepository.findAll());

        if (id != null) {
            UserHasSkill byId = userHasSkillRepository.findById(id).orElse(new UserHasSkill());
            model.addAttribute("userHasSkill", byId);
            log.debug("modified UserHasSkill based on existing id: {}",id);
        } else {
            model.addAttribute("userHasSkill", new DtoUserHasSkill());
            log.debug("Added to Spring model new DtoUserHasSkill");
        }
        return "binded-list-form";
    }

    @GetMapping("/delete-bind/{id}")
    public String deleteFromUserHasSkill(@PathVariable Long id) {
        userHasSkillRepository.deleteById(id);
        log.debug("Bind was deleted");
        return "redirect:/binded-list";
    }

    @PostMapping("/bindedFormProcess")
    public String bindedFormProcess(DtoUserHasSkill dtoUserHasSkill, Model model) {
        UserHasSkill userHasSkill = new UserHasSkill();
        userHasSkill.setId(dtoUserHasSkill.getId());
        log.debug("Value of id from dto dtoUserHasSkill is: {} ", dtoUserHasSkill.getId());
        userHasSkill.setUser(dtoUserHasSkill.getUser());
        log.debug("Value of user from dto dtoUserHasSkill is: {} ", dtoUserHasSkill.getUser());
        userHasSkill.setSkill(dtoUserHasSkill.getSkill());
        log.debug("Value of skill from dto dtoUserHasSkill is: {} ", dtoUserHasSkill.getSkill());
        userHasSkillRepository.save(userHasSkill);
        return "redirect:/binded-list";

    }
}
