package cz.karmasin.myportfolio.controller;

import cz.karmasin.myportfolio.dto.DtoUserHasTemperature;
import cz.karmasin.myportfolio.model.entity.UserHasTemperature;
import cz.karmasin.myportfolio.model.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;


import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Controller
public class TemperatureController {

    private final UserHasTemperatureRepository userHasTemperatureRepository;
    private final TemperatureRepository temperatureRepository;
    private final UserRepository userRepository;


    public TemperatureController(UserHasTemperatureRepository userHasTemperatureRepository, TemperatureRepository temperatureRepository, UserRepository userRepository) {
        this.userHasTemperatureRepository = userHasTemperatureRepository;
        this.temperatureRepository = temperatureRepository;
        this.userRepository = userRepository;
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException() {
        return "error-id";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @GetMapping("/t-list")
    public String showAllUsersWithTemperature(Model model) {
        model.addAttribute("TemperatureList", userHasTemperatureRepository.findAll());
        log.debug("List of all users with temperature loaded");
        return "t-list";
    }

    @GetMapping(value = {"/t-form", "/t-form/{id}"})
    public String showBindedForm(@PathVariable(required = false) Long id, Model model) {

        model.addAttribute("userList", userRepository.findAll());

        if (id != null) {
            UserHasTemperature byId = userHasTemperatureRepository.findById(id).orElse(new UserHasTemperature());
            model.addAttribute("userHasTemperature", byId);
            log.debug("modified userHasTemperature based on existing id: {}",id);
        } else {
            model.addAttribute("userHasTemperature", new DtoUserHasTemperature());
            log.debug("Added to Spring model new DtoUserHasTemperature");
        }
        return "t-list-form";
    }

    @GetMapping("/delete-t/{id}")
    public String deleteFromUserHasTemperature(@PathVariable Long id) {
        userHasTemperatureRepository.deleteById(id);
        log.debug("Bind was deleted");
        return "redirect:/t-list";
    }

    @PostMapping("/bindedFormProcessT")
    public String bindedFormProcessT(DtoUserHasTemperature dtoUserHasTemperature) {
        UserHasTemperature userHasTemperature = new UserHasTemperature();

        userHasTemperature.setId(dtoUserHasTemperature.getId());
        log.debug("Value of id from dto dtoUserHasTemperature is: {} ", dtoUserHasTemperature.getId());

        userHasTemperature.setUser(dtoUserHasTemperature.getUser());
        log.debug("Value of user from dto dtoUserHasTemperature is: {} ", dtoUserHasTemperature.getUser());

        temperatureRepository.save(dtoUserHasTemperature.getTemperature()); //for solving transient error

        userHasTemperature.setTemperature(dtoUserHasTemperature.getTemperature());
        log.debug("Value of skill from dto dtoUserHasTemperature is: {} ", dtoUserHasTemperature.getTemperature());


        userHasTemperatureRepository.save(userHasTemperature);
        return "redirect:/t-list";

    }
}
