package cz.karmasin.myportfolio.controller;

import cz.karmasin.myportfolio.dto.DtoSkill;
import cz.karmasin.myportfolio.model.repository.SkillRepository;
import cz.karmasin.myportfolio.service.SkillService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Slf4j
@Controller
public class SkillController {

    private final SkillRepository skillRepository;
    private final SkillService skillService;

    public SkillController(SkillRepository skillRepository, SkillService skillService) {
        this.skillRepository = skillRepository;
        this.skillService = skillService;
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException() {
        return "error-id";
    }


    @GetMapping("/skill-list")
    public String showAllSkills(Model model) {
        skillService.showAllSkillsService(model);
        return "skill-list";
    }



    @GetMapping(value = {"/skill-form", "/skill-form/{id}"})
    public String showProductForm(@PathVariable(required = false) Long id, Model model) {
        skillService.showProductFormService(id,model);
        return "skill-form";
    }

    @GetMapping("/delete-skill/{id}")
    public String deleteFromSkill(@PathVariable Long id) {
        String deleteSkill = skillRepository.findById(id).toString();
        skillRepository.deleteById(id);
        log.debug("Skill {} was deleted",deleteSkill);
        return "redirect:/skill-list";
    }

    @PostMapping("/skillFormProcess")
    public String skillFormProcess(DtoSkill dtoSkill) {
        skillService.skillFormProcessService(dtoSkill);
        return "redirect:/skill-list";

    }
}
