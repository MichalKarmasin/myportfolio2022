package cz.karmasin.myportfolio.controller;
import cz.karmasin.myportfolio.dto.DtoUser;
import cz.karmasin.myportfolio.model.entity.User;
import cz.karmasin.myportfolio.model.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException() {
        return "error-id";
    }


    @GetMapping("/user-list")
    public String showAllUsers(Model model) {
        model.addAttribute("userList", userRepository.findAll());
        log.debug("List of all users loaded");
        return "user-list";
    }

    @GetMapping(value = {"/user-form", "/user-form/{id}"})
    public String showUserForm(@PathVariable(required = false) Long id, Model model) {
        if (id != null) {
            User byId = userRepository.findById(id).orElse(new User());
            model.addAttribute("user", byId);
            log.debug("modified user based on existing id: {}",id);
        } else {
            model.addAttribute("user", new DtoUser());
            log.debug("Added to Spring model new DtoUser");
        }
        return "user-form";
    }

    @GetMapping("/delete-user/{id}")
    public String deleteFromUser(@PathVariable Long id) {
        String deleteUser = userRepository.findById(id).toString();
        userRepository.deleteById(id);
        log.debug("User {} was deleted",deleteUser);
        return "redirect:/user-list";
    }

    @PostMapping("/userFormProcess")
    public String skillFormProcess(DtoUser dtoUser) {
        User user = new User();
        user.setId(dtoUser.getId());
        user.setFirstName(dtoUser.getFirstName());
        user.setLastName(dtoUser.getLastName());
        user.setUserName(dtoUser.getUserName());
        userRepository.save(user);
        log.debug("User {} was saved",user.toString());
        return "redirect:/user-list";

    }
}
