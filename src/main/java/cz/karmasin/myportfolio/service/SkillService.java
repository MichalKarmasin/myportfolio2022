package cz.karmasin.myportfolio.service;

import cz.karmasin.myportfolio.dto.DtoSkill;
import cz.karmasin.myportfolio.model.entity.Skill;
import cz.karmasin.myportfolio.model.repository.SkillRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

@Slf4j
@Service
public class SkillService {


    private final SkillRepository skillRepository;

    public SkillService(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    public void showAllSkillsService(Model model) {
        model.addAttribute("skillList", skillRepository.findAll());
        log.debug("List of all skills loaded");

    }

    public Iterable<Skill> showAllSkillsServiceWithoutModel() {
        return skillRepository.findAll();
    }

    public void showProductFormService(@PathVariable(required = false) Long id, Model model){

        if (id != null) {
            Skill byId = skillRepository.findById(id).orElse(new Skill());
            model.addAttribute("skill", byId);
            log.debug("modified skill based on existing id: {}",id);
        } else {
            model.addAttribute("skill", new DtoSkill());
            log.debug("Added to Spring model new DtoSkill");
        }

        log.debug("Form was loaded OK");

    }

    public void skillFormProcessService(DtoSkill dtoSkill) {
        Skill skill = new Skill();
        if (dtoSkill.getId() != null) {
            skill.setId(dtoSkill.getId());
        }

        skill.setName(dtoSkill.HighLightNameText(dtoSkill.getName()));
        skill.setLevel(dtoSkill.getLevel());
        skillRepository.save(skill);
        log.debug("Skill {} was saved",skill.toString());


    }

}
