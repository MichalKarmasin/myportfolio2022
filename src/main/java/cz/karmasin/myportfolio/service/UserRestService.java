package cz.karmasin.myportfolio.service;
import cz.karmasin.myportfolio.model.entity.ResultTemp;
import cz.karmasin.myportfolio.model.entity.Temperature;
import cz.karmasin.myportfolio.model.repository.TemperatureRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


@Slf4j
@Service
public class UserRestService {
    private final TemperatureRepository temperatureRepository;

    public UserRestService(TemperatureRepository temperatureRepository) {
        this.temperatureRepository = temperatureRepository;
    }

    public Integer getSumDaysForTemperature(int tempA, int tempB) throws ParseException {
        Iterable<Temperature> temperatures = temperatureRepository.findAllByOrderByDateInAsc();
        log.debug("Set temps are {}", tempA+ " " + tempB );
        int longestDaysInterval  = 0;

        for (Temperature t: temperatures) {
            if ((t.getValue() >= tempA) && (t.getValue() <= tempB)) {
                log.debug("DateIn value from db {}", t.getDateIn());
                longestDaysInterval++;
            } else {
                continue;
            }

            log.debug("value of variable longestDaysInterval {}", longestDaysInterval);
        }
        return longestDaysInterval;
    }

    public List<ResultTemp> getDaysInLongestInterval(int tempA, int tempB) throws ParseException {
            Iterable<Temperature> temperatures = temperatureRepository.findAllByOrderByDateInAsc();
            List<ResultTemp> result = new ArrayList<>();

            SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd");
            isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            for (Temperature t : temperatures) {
                if ((t.getValue() >= tempA) && (t.getValue() <= tempB)) {
                    log.debug("DateIn value from db {}", t.getDateIn());
                    ResultTemp resultTemp = new ResultTemp();
                    Date date = isoFormat.parse(t.getDateIn().toString());
                    resultTemp.setDate(date);
                    resultTemp.setValue(t.getValue());
                    result.add(resultTemp);
                } else {
                    continue;
                }

            }

            return result;
        }
}
