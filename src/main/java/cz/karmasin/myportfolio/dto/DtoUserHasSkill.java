package cz.karmasin.myportfolio.dto;


import cz.karmasin.myportfolio.model.entity.Skill;
import cz.karmasin.myportfolio.model.entity.User;
import lombok.Data;

/**
 * The DTO class DtoUserHasSkill for transfer data from thymeleaf form to Spring controller
 *
 * @author Michal Karmasín
 */
@Data
public class DtoUserHasSkill {
    private long id;
    private Skill skill;
    private User user;

}
