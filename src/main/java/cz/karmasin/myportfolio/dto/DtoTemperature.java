package cz.karmasin.myportfolio.dto;

import lombok.Data;
import java.util.Date;

@Data
public class DtoTemperature {

    private long id;
    private int value;
    private Date dateIn;
    private String timeIn;
}
