package cz.karmasin.myportfolio.dto;


import cz.karmasin.myportfolio.model.entity.Levels;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * The DTO class DtoSkill for transfer data from thymeleaf form to Spring controller
 *
 * @author Michal Karmasín
 */



@Data
public class DtoSkill {
    private Long id;
    private String name;
    private Levels level;


    public String HighLightNameText(String name){

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

        return name.toLowerCase() + " " + timeStamp;

    }
}
