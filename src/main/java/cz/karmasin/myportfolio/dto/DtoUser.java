package cz.karmasin.myportfolio.dto;


import lombok.Data;

/**
 * The DTO class DtoUser for transfer data from thymeleaf form to Spring controller
 *
 * @author Michal Karmasín
 */
@Data
public class DtoUser {
    private long id;
    private String firstName;
    private String lastName;
    private String userName;
}
