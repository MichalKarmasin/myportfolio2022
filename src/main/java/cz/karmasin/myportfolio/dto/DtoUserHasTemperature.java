package cz.karmasin.myportfolio.dto;

import cz.karmasin.myportfolio.model.entity.Temperature;
import cz.karmasin.myportfolio.model.entity.User;
import lombok.Data;

@Data
public class DtoUserHasTemperature {
    private long id;
    private Temperature temperature;
    private User user;
}
