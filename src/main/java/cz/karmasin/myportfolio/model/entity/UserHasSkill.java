package cz.karmasin.myportfolio.model.entity;

import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * The Entity UserHasSkill which have field id, skill and user,
 * there is relations  @ManyToOne on Skill and User entity
 *
 * @author Michal Karmasín
 */
@Entity
@Data
public class UserHasSkill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Skill skill;

    @ManyToOne
    private User user;
}
