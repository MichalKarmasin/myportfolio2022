package cz.karmasin.myportfolio.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import org.hibernate.annotations.CascadeType;

import javax.persistence.*;


@Entity
@Data
public class UserHasTemperature {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonBackReference //solving infinite json recursion at output
    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    private Temperature temperature;

    @JsonBackReference //solving infinite json recursion at output
    @ManyToOne
    private User user;
}
