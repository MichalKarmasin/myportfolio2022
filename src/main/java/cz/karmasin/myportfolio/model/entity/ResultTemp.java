package cz.karmasin.myportfolio.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ResultTemp {

        @JsonFormat(pattern="yyyy-MM-dd")
        Date date;
        int value;

}
