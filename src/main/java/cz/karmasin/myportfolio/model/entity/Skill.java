package cz.karmasin.myportfolio.model.entity;

import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;


/**
 * The Entity Skill which have field id, name and Enum level,
 * there is Set relations for skillsInUsers
 *
 * @author Michal Karmasín
 */
@Entity
@Table(name = "Skill")
@Data
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "NAME")
    private String name;
    @Enumerated(EnumType.STRING)
    private Levels level;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY)
    private Set<UserHasSkill> skillsInUsers;

    public Skill(long id, String NAME, Levels level) {

    }

    public Skill() {

    }
}

