package cz.karmasin.myportfolio.model.entity;

import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * The Entity User which have field id, firstName and lastName,
 * there is Set relations for userHasSkills
 *
 * @author Michal Karmasín
 */
@Entity
@Table(name = "Users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "USERNAME")
    private String userName;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY)
    private Set<UserHasSkill> userHasSkills;

    @OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserHasTemperature> userHasTemperature;

}
