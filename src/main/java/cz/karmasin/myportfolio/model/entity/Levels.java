package cz.karmasin.myportfolio.model.entity;

public enum Levels {
    BASIC,
    ADVANCED,
    EXPERT
}
