package cz.karmasin.myportfolio.model.repository;

import cz.karmasin.myportfolio.model.entity.Temperature;
import cz.karmasin.myportfolio.model.entity.User;
import org.springframework.data.repository.CrudRepository;



public interface TemperatureRepository extends CrudRepository<Temperature, Long> {

    Iterable<Temperature> findAllByOrderByDateInAsc();

}
