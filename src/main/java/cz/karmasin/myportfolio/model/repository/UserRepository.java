package cz.karmasin.myportfolio.model.repository;

import cz.karmasin.myportfolio.model.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    @EntityGraph(attributePaths = {"userHasSkills"})
    @Override
    Optional<User> findById(Long id);
}
