package cz.karmasin.myportfolio.model.repository;
import cz.karmasin.myportfolio.model.entity.Temperature;
import cz.karmasin.myportfolio.model.entity.UserHasTemperature;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserHasTemperatureRepository extends CrudRepository<UserHasTemperature, Long> {

}
