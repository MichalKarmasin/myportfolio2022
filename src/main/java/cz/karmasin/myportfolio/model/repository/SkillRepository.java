package cz.karmasin.myportfolio.model.repository;

import cz.karmasin.myportfolio.model.entity.Skill;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

public interface SkillRepository extends CrudRepository<Skill, Long> {

    @EntityGraph(attributePaths = {"skillsInUsers"})
    Skill findSkillByNameContains(String name);

}
