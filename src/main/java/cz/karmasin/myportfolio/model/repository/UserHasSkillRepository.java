package cz.karmasin.myportfolio.model.repository;

import cz.karmasin.myportfolio.model.entity.UserHasSkill;
import org.springframework.data.repository.CrudRepository;

public interface UserHasSkillRepository extends CrudRepository<UserHasSkill, Long> {

}
