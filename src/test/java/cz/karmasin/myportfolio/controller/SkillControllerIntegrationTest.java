package cz.karmasin.myportfolio.controller;

import cz.karmasin.myportfolio.model.entity.Levels;
import cz.karmasin.myportfolio.model.entity.Skill;
import cz.karmasin.myportfolio.model.repository.SkillRepository;
import cz.karmasin.myportfolio.service.SkillService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = SkillController.class) //there is need class which we want to test
public class SkillControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SkillService skillService;

    @MockBean
    private SkillRepository skillRepository;

    @Test
    @Order(1)
    void testStatusShowAllSkillsTest() throws Exception {
        //given

        //when
       MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.get("/skill-list")
                .contentType(MediaType.TEXT_HTML))
                .andReturn().getResponse();
        log.debug(String.valueOf(response.getStatus()));
        log.debug(response.getContentAsString());

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

    }

    @Test
    @Order(2)
    void deleteFromSkill() throws Exception {

            //given
            final Skill skill = new Skill(1L, "CSS", Levels.BASIC);
            Optional<Skill> optionalSkill = Optional.of(skill);
            Mockito.when(skillRepository.findById(1L)).thenReturn(optionalSkill);

            // when
             skillRepository.delete(skill);


            // then
            Mockito.verify(skillRepository, times(1)).delete(skill);

    }

    @Test
    @Order(3)
    void deleteFromSkillUrl() throws Exception {

        //given

        // when
         ResultActions responseResultAction = mockMvc.perform(MockMvcRequestBuilders.get("/delete-skill/{id}/","2"));

        // then

        responseResultAction
                .andDo(print())
                .andExpect(status().is3xxRedirection());

    }


}