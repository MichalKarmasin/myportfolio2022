package cz.karmasin.myportfolio.service;

import cz.karmasin.myportfolio.dto.DtoSkill;
import cz.karmasin.myportfolio.model.entity.Levels;
import cz.karmasin.myportfolio.model.entity.Skill;
import cz.karmasin.myportfolio.model.repository.SkillRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
/** thanks for that we don't type code bellow
 * private AutoCloseable autoCloseable;
 *
 * and code for autoCloseable in @BeforeEach
 * autoCloseable = MockitoAnnotations.openMocks(this);
 *
 * and whole method
 *  @AfterEach
 *     void tearDown() throws Exception {
 *         autoCloseable.close();
 *     }
 *
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SkillServiceTest {

    /**
     * We have tested this repository, and we know that is works,
     * so we don't need real instance of this repository, we will satisfy
     * with mock, and benefit is so, that our test is fast*/
    @Mock
    private SkillRepository skillRepository;
    @Mock
    private Model model;

    private SkillService skillServiceUnderTest;

    @BeforeEach
    void setUp() {
        skillServiceUnderTest = new SkillService(skillRepository);
    }

    @Test
    @Order(1)
    void CanShowAllSkillsServiceWithoutModel() {
        //when
        skillServiceUnderTest.showAllSkillsServiceWithoutModel();
        //then
        verify(skillRepository).findAll();
    }


    @Test
    @Order(2)
    void CanShowAllSkillsServiceWithModel() {
        //when
        skillServiceUnderTest.showAllSkillsService(model);
        //then
        verify(skillRepository).findAll();
    }


    @Test
    @Order(3)
    void CanShowProductFormService() {

        //given
        Long id = 1L;
        Skill skill = new Skill();
        skill.setId(1L);
        skill.setName("CSS");
        skill.setLevel(Levels.BASIC);
        Skill byId = skill;

        //when
        skillRepository.save(skill);
        if (id != null) {
            byId = skillRepository.findById(id).orElse(new Skill());
            model.addAttribute("skill", byId);
        } else {
            model.addAttribute("skill", new DtoSkill());
        }
        //then
        verify(skillRepository).save(skill);

        if (id != null) {
            verify(skillRepository).findById(id);
            verify(model).addAttribute("skill", byId);
        } else {
            verify(model).addAttribute("skill", new DtoSkill());
        }


    }

    @Test
    @Order(4)
    public void skillFormProcessServiceAddingNewSkillTest() {
        //given
        Long id = 1L;
        DtoSkill dtoSkill = new DtoSkill();
        dtoSkill.setId(id);
        dtoSkill.setName("CSS");
        dtoSkill.setLevel(Levels.BASIC);
        Skill skill = new Skill();

        //when
        if (dtoSkill.getId() != null) {
            skill.setId(dtoSkill.getId());
        }
        skill.setName(dtoSkill.getName());
        skill.setLevel(dtoSkill.getLevel());
        skillRepository.save(skill);

        //then
        ArgumentCaptor<Skill> skillArgumentCaptor = ArgumentCaptor.forClass(Skill.class);
        verify(skillRepository).save(skillArgumentCaptor.capture());

        Skill capturedSkill = skillArgumentCaptor.getValue();
        assertThat(capturedSkill).isEqualTo(skill);


    }



}