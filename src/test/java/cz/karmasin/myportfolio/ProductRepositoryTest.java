package cz.karmasin.myportfolio;

import cz.karmasin.myportfolio.model.entity.Skill;
import cz.karmasin.myportfolio.model.entity.Levels;
import cz.karmasin.myportfolio.model.entity.User;
import cz.karmasin.myportfolio.model.entity.UserHasSkill;
import cz.karmasin.myportfolio.model.repository.SkillRepository;
import cz.karmasin.myportfolio.model.repository.UserHasSkillRepository;
import cz.karmasin.myportfolio.model.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
class ProductRepositoryTest {

    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserHasSkillRepository userHasSkillRepository;

    @Test
    void saveProductTest() {
        Skill skill = new Skill();
        Skill skill2 = new Skill();
        Skill skill3 = new Skill();
        skill.setId(1L);
        skill.setName("CSS");
        skill.setLevel(Levels.BASIC);

        skill2.setId(2L);
        skill2.setName("HTML");
        skill2.setLevel(Levels.ADVANCED);

        skill3.setId(3L);
        skill3.setName("Ruby");
        skill3.setLevel(Levels.EXPERT);

        skillRepository.save(skill);
        skillRepository.save(skill2);
        skillRepository.save(skill3);

    }

    @Test
    void loadProductTest() {
        Iterable<Skill> listAllSkills = skillRepository.findAll();
        Skill skillContainName = skillRepository.findSkillByNameContains("CSS");

    }

    @Test
    void saveComplete() {
        Skill skill = new Skill();
        User user = new User();
        UserHasSkill userHasSkill = new UserHasSkill();

        skill.setName("SQL");
        skill.setLevel(Levels.BASIC);
        skillRepository.save(skill);
        user.setFirstName("Pepa");
        user.setLastName("Novák");
        userRepository.save(user);

        userHasSkill.setSkill(skill);
        userHasSkill.setUser(user);
        userHasSkillRepository.save(userHasSkill);

    }


    @Test
    void getAllUsers() {
        Optional<User> all = userRepository.findById(1L);
    }
}
