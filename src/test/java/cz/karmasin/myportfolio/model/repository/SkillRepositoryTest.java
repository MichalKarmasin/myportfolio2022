package cz.karmasin.myportfolio.model.repository;

import cz.karmasin.myportfolio.model.entity.Levels;
import cz.karmasin.myportfolio.model.entity.Skill;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DataJpaTest
class SkillRepositoryTest {

    Skill skillTest = new Skill();
    String testName;


    @Autowired
    public SkillRepository skillRepositoryUnderTest;

    @Test
    @Order(2)
    void itShouldFindSkillByNameContains() {
        //given
        testName = "CSS";
        skillTest.setName(testName);
        skillTest.setLevel(Levels.BASIC);
        skillRepositoryUnderTest.save(skillTest);
        //when
        Skill testFindSkill = skillRepositoryUnderTest.findSkillByNameContains(testName);
        //then
        assertThat(testFindSkill.getName()).isEqualTo(testName);
    }

    @Test
    @Order(1)
    void itShouldFindSkillByNameContainsTestFailedInput() {
        //given
        testName = "CSS";
        skillTest.setName(testName);
        skillTest.setLevel(Levels.BASIC);
        skillRepositoryUnderTest.save(skillTest);
        String testNameFailed = "HTML";
        //when
        Skill testFindSkill = skillRepositoryUnderTest.findSkillByNameContains(testName);
        //then
        /**
         * Testing failed result of test*/
        assertThat(testFindSkill.getName()).isNotEqualTo(testNameFailed);
    }

    @AfterEach
    void tearDown() {
        skillRepositoryUnderTest.deleteAll();
    }

}

